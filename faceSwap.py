#!/usr/bin/env python

'''
face detection using haar cascades

USAGE:
    facedetect.py [--cascade <cascade_fn>] [--nested-cascade <cascade_fn>] [<video_source>]

2016 October 28
Modified from the sample that shipped with OpenCV to use the 
Raspberry Pi camera using the method from 
http://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/

Seth McNeill
Union College
'''

# Python 2/3 compatibility, print has to be called as a function now (print (a))
from __future__ import print_function

import numpy as np
import cv2
# added for Raspberry Pi camera module interface
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import random

from sense_hat import SenseHat


# local modules
from video import create_capture
from common import clock, draw_str


def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(20, 20),
                                     flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

if __name__ == '__main__':
    import sys, getopt
    print(__doc__)
    sense = SenseHat() # initialize the SenseHat

    args, video_src = getopt.getopt(sys.argv[1:], '', ['cascade=', 'nested-cascade=', 'smiley='])
#    args = getopt.getopt(sys.argv[1:], '', ['cascade=', 'nested-cascade='])
#    try:
#        video_src = video_src[0]
#    except:
#        video_src = 0

    for a in args:
      print(a)
    args = dict(args)
    cascade_fn = args.get('--cascade', "../../data/haarcascades/haarcascade_frontalface_alt.xml")
    nested_fn  = args.get('--nested-cascade', "../../data/haarcascades/haarcascade_eye.xml")
    smileyFile = args.get('--smiley', './smiley.jpg')

    cascade = cv2.CascadeClassifier(cascade_fn)
    nested = cv2.CascadeClassifier(nested_fn)
    smileyFace = cv2.imread(smileyFile)

#    cam = create_capture(video_src, fallback='synth:bg=../data/lena.jpg:noise=0.05')

    # initialize the camera and grab a reference to the raw camera capture
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 32
    rawCapture = PiRGBArray(camera, size=(640, 480))
 
    # allow the camera to warmup
    time.sleep(0.1)

    fNum = 0 # face number for debugging
# capture frames from the camera
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        t = clock()
	# grab the raw NumPy array representing the image, then initialize the timestamp
	# and occupied/unoccupied text
	imgRaw = frame.array
  # flip the image so it looks like a mirror
	img = cv2.flip(imgRaw, 1)

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        rects = detect(gray, cascade)
        # make the senseHat show how many faces are seen
        if(len(rects) > 0):
          sense.show_letter(str(len(rects)), [0,255,0],[0,0,0])
        else:
          sense.clear(random.randint(0,255),random.randint(0,255),random.randint(0,255))
        vis = img.copy()
#        draw_rects(vis, rects, (0, 255, 0))

        # copied idea from C++ FaceSwap program
        prevFace = []
        for x1, y1, x2, y2 in rects:
          curROI = vis[y1:y2, x1:x2] # get ROI before being drawn over
          curFace = curROI.copy() # get a deep copy so it won't be overwritten
          #cv2.rectangle(vis, (x1, y1), (x2, y2), (255,0,0), 2) # rectangle around face

# choose replacement face
          if(prevFace == []): # this is the first face
            firstFaceRect = [x1, y1, x2, y2]
            firstFace = curFace
#            replacementFace = np.zeros([x2-x1, y2-y1, 3])
#            replacementFace[:,:,2] = 255 # blue rectangle
            #replacementFace = cv2.resize(smileyFace, (x2-x1, y2-y1), interpolation = cv2.INTER_CUBIC)
#            prevFace = cv2.resize(smileyFace, (x2-x1, y2-y1), interpolation = cv2.INTER_CUBIC)
            prevFace = curFace
            continue
          else: # not the first face
            print("Resizing prevFace ", fNum)
            fNum = fNum + 1
            replacementFace = cv2.resize(prevFace, (x2-x1, y2-y1), interpolation = cv2.INTER_CUBIC)

# create a mask
            maskr = np.full((x2-x1, y2-y1), 0.0) # blank mask
            # draw a filled oval on the mask
            cv2.ellipse(maskr, (abs(x2-x1)/2,abs(y2-y1)/2), \
              (abs(x2-x1)/2-5,abs(y2-y1)/2-5), 0,0,360, 1.0, -1)
            mask = cv2.blur(maskr, (20,20))
            mask3 = np.stack([mask]*3, axis=2) # make RGB mask
#            cv2.imshow('mask1',mask3)
  # mix the replacement face and the background image
            vis[y1:y2, x1:x2] = mask3*replacementFace + \
               (1.0 - mask3)*vis[y1:y2,x1:x2]
            #vis[y1:y2, x1:x2] = replacementFace
  #          cv2.ellipse(vis, ((x1+x2)/2, (y1+y2)/2), (abs(x2-x1)/2, abs(y2-y1)/2),0,0,360, (255,0,0), 2) # ellipse around face
            #cv2.imshow('replacementFace', replacementFace)
            #cv2.imshow('curFace', curFace)
            prevFace = curFace
        # if more than one face, need to go back and change first face
        if(rects != [] and rects.shape[0] > 1):
          print("resizing prefFace for firstFace")
          maskr = np.full((firstFaceRect[2] - firstFaceRect[0], \
            firstFaceRect[3] - firstFaceRect[1]), 0.0) # blank mask
          # draw a filled oval on the mask
          cv2.ellipse(maskr, (abs(firstFaceRect[2] - firstFaceRect[0])/2, \
            abs(firstFaceRect[3] - firstFaceRect[1])/2), \
            (abs(firstFaceRect[2] - firstFaceRect[0])/2-5, \
            abs(firstFaceRect[3] - firstFaceRect[1])/2-5), 0,0,360, 1.0, -1)
          mask = cv2.blur(maskr, (20,20))
          mask3 = np.stack([mask]*3, axis=2) # make RGB mask
#          cv2.imshow('mask',mask3)
          replacementFace = cv2.resize(prevFace, \
            (firstFaceRect[2] - firstFaceRect[0], firstFaceRect[3] - firstFaceRect[1]), \
            interpolation = cv2.INTER_CUBIC)
          vis[firstFaceRect[1]:firstFaceRect[3], firstFaceRect[0]:firstFaceRect[2]] = \
            mask3*replacementFace + (1.0 - mask3)*vis[firstFaceRect[1]:firstFaceRect[3],\
            firstFaceRect[0]:firstFaceRect[2]]
 
        # future would be to use an aliased oval mask in changing faces
#        if not nested.empty():
#            for x1, y1, x2, y2 in rects:
#                roi = gray[y1:y2, x1:x2]
#                vis_roi = vis[y1:y2, x1:x2]
#                subrects = detect(roi.copy(), nested)
#                draw_rects(vis_roi, subrects, (255, 0, 0))
        dt = clock() - t

        draw_str(vis, (20, 20), 'time: %.1f ms' % (dt*1000))
        sVis = vis.shape
        largevis = cv2.resize(vis, (sVis[1]*2, sVis[0]*2), cv2.INTER_CUBIC)
        cv2.imshow('facedetect', largevis)

	# clear the stream in preparation for the next frame
	rawCapture.truncate(0)

        if 0xFF & cv2.waitKey(5) == 27:
            break
    cv2.destroyAllWindows()
    sense.clear() # turn off all the LEDs on the SenseHat
